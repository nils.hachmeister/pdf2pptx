from pptx import Presentation
from pptx.util import Inches, Cm
from pdf2image import convert_from_path
import os

from tkinter import filedialog
from tkinter import *

from PyPDF2 import PdfFileReader

root = Tk()
root.withdraw()
pdf_file = filedialog.askopenfilename(initialdir = os.path.expanduser("~"),
                                           title="Select pdf-file to convert",
                                           filetypes=(("PDF-File", "*.pdf"), ("all files", "*.*")))
root.destroy()

box = PdfFileReader(open(pdf_file, "rb")).getPage(0).mediaBox

path, file = os.path.split(pdf_file)
bare_file_name = os.path.splitext(file)[0]

print("Start converting pdf slides to images. This might take a moment.")

pages = convert_from_path(pdf_file, 500)

print("Done converting.")

img_dir_name = "{0}_imgs".format(bare_file_name)
print("Saving image files to", img_dir_name)
os.makedirs(os.path.join(path, img_dir_name))
img_files = []
for idx, page in enumerate(pages):
    print("Saving image of slide", idx+1)
    img_files.append(os.path.join(path, img_dir_name, '{0}_{1}_out.png'.format(os.path.splitext(file)[0], idx)))
    page.save(img_files[-1], 'PNG')

print("Done converting pdf-slide to images.")
print("Start constructing the power point presentation.")

prs = Presentation()
quotient = float(box.getWidth()) / float(box.getHeight())
prs.slide_height = Cm(19.5)
prs.slide_width = Cm(19.5 * quotient)
blank_slide_layout = prs.slide_layouts[6]

for idx, img_file in enumerate(img_files):
    print("Constructing slide", idx+1)
    slide = prs.slides.add_slide(blank_slide_layout)
    left = top = Cm(0)
    pic = slide.shapes.add_picture(img_file, left, top, height=prs.slide_height)

print("Done constructing power point presentation.")
save_path = os.path.join(path,"{0}.pptx".format(bare_file_name))
print("Saving to", save_path)

prs.save(save_path)

print("All done.")