# pdf2pptx

This mini-project is about a python script which can be used to convert 
pdf-slides to pptx slides. The resulting slides are, however, basically images 
of the pdfs put into a Powerpoint presentation, which mean, the slides cannot be
edited.

## Usage
When executing a script a file dialog will appear, prompting you to choose a pdf 
file to convert. Afterwards an directory will be created besides your chosen pdf
file. Within that directory image files (.png) will be placed. Once done, a pptx
will be generated containing one slide per pdf-slide with an image of the pdf-
slide strething over the entire Power Point slide.

## Dependencies
The script uses PyPDF2, python-pptx and pdf2image as well as tkinter. Using pip
it should be easy to fullfill these requirements.

## Disclaimers/TODOs

The resulting Powerpoint slides will keep the aspect ration of the pdf, but
the height will be fixed at 19.5cm. Also it is assumed that all pdf pages have
the same size.

There are known issues with certain pdfs for which pdf2image generates white 
border and the determination of the aspect ration of the pdf slides does not
work. This seems to be the case, e.g. when the pdf has been produced by 
using the print to file function of Adobe.

The fact that the image files are being saved is somewhat unnecessary. This
might be removed in the future.